import { BrowserRouter, useRoutes } from 'react-router-dom';
import './App.scss';
import AppLayout from './components/AppLayout';
import CounterLayout from './components/Counter/CounterLayout';
import AutoCounterLayout from './components/auto-counter/AutoCounterLayout';
import ErrorBoundary from './components/error-boundary/ErrorBoundary';

const Router = () => {
  const routesUsingObj = [
    {
      path: '/',
      element: <AppLayout />,
      children: [
        {
          path: 'counter',
          element: <CounterLayout />,
          errorElement: <ErrorBoundary />
        },
        {
          path: 'auto-counter',
          element: <AutoCounterLayout />
        }
      ]
    }
  ];

  const routeElements = useRoutes(routesUsingObj);
  return routeElements;
};


function App() {
  return (
    <BrowserRouter>
      <Router />
    </BrowserRouter>
  );
}

export default App;
