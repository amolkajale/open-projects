import { counterContext } from "./CounterContex";

import React, { useReducer } from 'react'

function CounterProvider({ children }) {
    const initialState = {
        count: 0
    };
    const reducer = (state, action) => {
        switch (action.type) {
            case 'INCREAMENT_COUNT':
                return { ...state, count: state.count + 1 };
            case 'DECREAMENT_COUNT':
                return { ...state, count: state.count - 1 };
            default:
                return state;
        }
    };
    const [count, dispatch] = useReducer(reducer, initialState);
    return (
        <counterContext.Provider value={{ count, dispatch }}>
            {children}
        </counterContext.Provider>
    )
}

export default CounterProvider