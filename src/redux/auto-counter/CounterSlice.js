import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

const initialState = {
    count: 0
};
export const fetchUsers = createAsyncThunk('user/fetch-user', () => {
    let p = new Promise((res, rej) => {
        setTimeout(() => {
            res();
        }, 5000)
    })
    return p;
});

export const fetchUsers1 = createAsyncThunk('user/fetch-user1', () => {
    let p = new Promise((res, rej) => {
        setTimeout(() => {
            res();
        }, 5000)
    })
    return p;
});

export const counterSlice = createSlice({
    name: 'counter',
    initialState: initialState,
    reducers: {
        incrementCount: state => {
            state.count += 1;
        },

        resetCount: state => {
            state.count = 0;
        }
    },
    extraReducers: builder => {
        builder.addCase(fetchUsers.pending, () => {
            console.log("Pending")
        });
        builder.addCase(fetchUsers.fulfilled, () => {
            console.log("fulfilled")
        });
        builder.addCase(fetchUsers.rejected, () => {
            console.log("rejected")
        });

        builder.addCase(fetchUsers1.pending, () => {
            console.log("Pending1")
        });
        builder.addCase(fetchUsers1.fulfilled, () => {
            console.log("fulfilled1")
        });
        builder.addCase(fetchUsers1.rejected, () => {
            console.log("rejected1")
        });
    }

});

export const { incrementCount, resetCount } = counterSlice.actions;
export default counterSlice.reducer;