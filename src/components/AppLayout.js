import React from 'react'
import './AppLayout.scss'
import ProjectList from './ProjectList/ProjectList'
import { Outlet } from 'react-router-dom'
function AppLayout() {
  return (
    <div className='app-layout-container'>
      <div className='left-section'>
        <ProjectList />
      </div>
      <div>
        <Outlet></Outlet>
      </div>
    </div>
  )
}

export default AppLayout