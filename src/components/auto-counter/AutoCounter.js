import { Button } from '@mui/material';
import React, { useState } from 'react'
import './auto-counter.scss'
import { useSelector, useDispatch } from 'react-redux';
import { fetchUsers, fetchUsers1, incrementCount, resetCount } from '../../redux/auto-counter/CounterSlice';
function AutoCounter() {

    const counter = useSelector(x => x.counter);

    const dispatch = useDispatch();

    const [interval, setIntervalValue] = useState(null);
    const [displayValue, setDisplayValue] = useState('Start');

    const counterLogicFunc = () => {
        const interval = setInterval(() => {
            dispatch(incrementCount());
        }, 1000);
        setIntervalValue(interval);
    }

    const startCounter = () => {
        dispatch(resetCount());
        counterLogicFunc();
        setDisplayValue('Stop');
        dispatch(fetchUsers1())
    };

    const resumeCounter = () => {
        counterLogicFunc();
        setDisplayValue('Stop');
    };

    const stopCounter = () => {
        clearInterval(interval);
        setIntervalValue(null);
        setDisplayValue('Start')
    };

    const startStopCounter = () => {
        if (displayValue === 'Start') {
            startCounter();
        } else {
            stopCounter();
        }
    };
    if (counter.count === 5) {
        throw new Error('Error')
    } 
    return (
        <div>
            <h1>Counter: {counter.count}</h1>
            <div className='btn-auto-counter'>
                <Button variant="outlined" onClick={() => startStopCounter()}>{displayValue}</Button>
                &nbsp;<Button disabled={displayValue === 'Stop' ? true : false} variant="outlined" onClick={() => resumeCounter()}>Resume</Button>
            </div>
        </div >

    )
}

export default AutoCounter;