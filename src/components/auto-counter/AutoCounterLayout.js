import React from 'react'
import AutoCounter from './AutoCounter'
import { Provider } from 'react-redux'
import { store } from '../../redux/auto-counter/store'
import ErrorBoundary from '../error-boundary/ErrorBoundary'


function AutoCounterLayout() {
    return (
        <ErrorBoundary>
            <Provider store={store}>
                <AutoCounter />
            </Provider>
        </ErrorBoundary>

    )
}

export default AutoCounterLayout