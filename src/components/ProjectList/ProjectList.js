import React from 'react'
import Counter from '../Counter/Counter';
import './ProjectList'
import { NavLink } from 'react-router-dom';

function ProjectList() {
    const projectLists = [
        {
            name: 'Counter',
            link: '/counter',
            id: 1
        },
        {
            name: 'Auto Counter',
            link: '/auto-counter',
            id: 2
        }
    ];
    return (
        <div className='project-list'>
            <ul>
                {projectLists.map((project) => (
                    <li key={project.id}><NavLink to={project.link}>{project.name}</NavLink></li>
                ))}
            </ul>
        </div>
    )
}

export default ProjectList