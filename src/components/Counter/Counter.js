import React, { useContext } from 'react'
import { counterContext } from '../../context/counter/CounterContex'
import { Button } from '@mui/material';

import './Counter.scss'

function Counter() {
    const { count, dispatch } = useContext(counterContext);
    return (
        <div className='counter-container'>
            <h1>Count:{count ? count.count : 0}</h1>
            <Button variant="outlined" onClick={() => dispatch({ type: 'INCREAMENT_COUNT' })}>Increase Count</Button>
            &nbsp;&nbsp;<Button variant="outlined" onClick={() => dispatch({ type: 'DECREAMENT_COUNT' })}>Decrease Count</Button>
        </div>
    )
}

export default Counter