import React from 'react'
import CounterProvider from '../../context/counter/CounterProvider'
import Counter from './Counter'

function CounterLayout() {
    return (
        <CounterProvider>
            <Counter />
        </CounterProvider>
    )
}

export default CounterLayout